import requests
import sys
import json

if len (sys.argv) > 1 and sys.argv[1] == "local":
      back = "http://localhost:8080/from_data"
      front = "http://localhost:8888/grew_web"
else:
      back = "http://back.grew.fr/from_data"
      front = "http://transform.grew.fr"

conll = """# sent_id = fr-ud-dev_00243
# text = Ils sont sept.
1	Ils	il	PRON	_	Gender=Masc|Number=Plur|Person=3|PronType=Prs	2	subj	_	wordform=ils
2	sont	être	AUX	_	Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_
3	sept	sept	NUM	_	Number=Plur	2	comp:pred	_	SpaceAfter=No
4	.	.	PUNCT	_	_	2	punct	_	_

"""

json_data="""
{
  "meta": { "sent_id": "fr-ud-dev_00243", "text": "Ils sont sept." },
  "nodes": {
    "0": { "form": "__0__" },
    "1": {
      "Gender": "Masc",
      "Number": "Plur",
      "Person": "3",
      "PronType": "Prs",
      "form": "Ils",
      "lemma": "il",
      "textform": "Ils",
      "upos": "PRON",
      "wordform": "ils"
    },
    "2": {
      "Mood": "Ind",
      "Number": "Plur",
      "Person": "3",
      "Tense": "Pres",
      "VerbForm": "Fin",
      "form": "sont",
      "lemma": "être",
      "textform": "sont",
      "upos": "AUX",
      "wordform": "sont"
    },
    "3": {
      "Number": "Plur",
      "SpaceAfter": "No",
      "form": "huit",
      "lemma": "huit",
      "textform": "huit",
      "upos": "NUM",
      "wordform": "huit"
    },
    "4": {
      "form": ".",
      "lemma": ".",
      "textform": ".",
      "upos": "PUNCT",
      "wordform": "."
    }
  },
  "edges": [
    { "src": "2", "label": "punct", "tar": "4" },
    { "src": "2", "label": { "1": "comp", "2": "pred" }, "tar": "3" },
    { "src": "2", "label": "subj", "tar": "1" },
    { "src": "0", "label": "root", "tar": "2" }
  ],
  "order": [ "0", "1", "2", "3", "4" ]
}
"""

grs = """
rule r { pattern { N[upos="AUX"] } commands { N.upos=VERB } }
"""

def test (data):
  response = requests.request("POST", back, data=data)
  json_reply= json.loads (response.text)

  if json_reply["status"] == "ERROR":
      print ("**************** ERROR ****************")
      print (json_reply["message"])
  elif json_reply["status"] == "OK":
      session_id = json.loads (response.text)["data"]["session_id"]
      print (f"{front}?session_id={session_id}")
  else:
      print (f"Unexpected answer: {json_reply}")

print ("========= conll + grs =========")
test ({ "conll": conll, "grs": grs })

print ("========= json + grs =========")
test ({ "json": json_data, "grs": grs })

print ("========= json =========")
test ({ "json": json_data})